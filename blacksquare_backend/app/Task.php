<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
     protected $table = 'task';
     public $timestamps = true;
     
     public function comments(){
          return $this->hasMany('App\Comments','task_id');
     }
}
