<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Http\Requests\tasksRequest;
use \App\Task;

use App\Events\UpdateTasks;

class taskController extends Controller
{
    public function index(){
        $tasks = Task::with(['comments' => function ($query){ 
            return $query->orderBy('id','desc');
        }])->orderBy('sort')->get()->groupBy('group_id');
        if(!isset($tasks[1])){  $tasks[1] = []; }
        if(!isset($tasks[2])){  $tasks[2] = []; }

        return response($tasks,200);
    }
    public function create(){
        
    }
    
    public function make_safe($variable) {
      $variable = strip_tags($variable);
      $variable = stripslashes($variable);
      $variable= trim($variable, "'");
        return $variable;
    }

    public function sort_tasks(Request $request){
        $tasks = $request->all();
        $sort = 0;
       foreach ($tasks as $key => $oldtasks){
           foreach($oldtasks as $oldtask){
            $task = Task::find($oldtask['id']);
            $task->sort = $sort;
            $task->group_id = $key;
            $task->save();
            $sort++;
           }
        }
        broadcast(new UpdateTasks())->toOthers();
        return response($tasks);
    }

    public function store(tasksRequest $request){
      
        $task_name = $request->input('task_name');
        $task_desc = $request->input('task_desc');
        $group_id = $request->input('group_id');
        $task_date = $request->input('date');
        $taskstatus = $request->input('status');
        $task  = new Task();
        $task->task_name = $this->make_safe($task_name);
        $task->task_desc = $this->make_safe($task_desc);
        $task->date = $this->make_safe($task_date);
        $task->group_id = $this->make_safe($group_id);
        $task->status = $this->make_safe($taskstatus);
        $task->save();
        $res =  array();
        $res["fail"] = "0";
        $res["type"] = "add";
        $res["task"] = $task;

        broadcast(new UpdateTasks())->toOthers();
      
        return response($res, 200);
    }
    public function show($id){
        $task = Task::find($id);
        $res = array();
        $res['fail']='0';
        $res['task']=$task;
        return response($res, 200);
    }
    public function edit($id){
        $task = Task::find($id);
        $res = array();
        $res['fail']='0';
        $res['task']=$task;
        return response($res, 200);
    }
    public function update(tasksRequest $request,$id){
        $task = Task::find($id);
        $task_name = $request->input('task_name');
        $task_desc = $request->input('task_desc');
        $group_id = $request->input('group_id');
        $task_date = $request->input('date');
        $taskstatus = $request->input('status');
     
        $task->task_name = $this->make_safe($task_name);
        $task->task_desc = $this->make_safe($task_desc);
        $task->date = $this->make_safe($task_date);
        $task->status = $this->make_safe($taskstatus);
        $task->save();
        $res = array();
        $res["fail"] = "0";
        $res["type"] = "edit";
        $res['task'] = $task;
        broadcast(new UpdateTasks())->toOthers();
        return response($res, 200);
    }
    public function destroy($id){
        $task = TAsk::find($id);
        $task->delete();
        broadcast(new UpdateTasks())->toOthers();
        return response('deleted successfully',200);
    }
}
