<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Http\Requests\commentsRequest;
use \App\Comments;
use \App\Task;
class commentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = new Comments();
        $comments = $comments->get();
        return response($comments,200);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(commentsRequest $request)
    {
        $comment = $request->input('comment');
        $task_id = $request->input('task_id');

        $comment = $this->make_safe ($request->input('comment'));
        $task_id =  $this->make_safe ($request->input('task_id'));

        $Comment = new Comments();
        $Comment->comment = $comment;
        $Comment->task_id = $task_id;
        $Comment->save();

        $task = Task::where('id', $task_id)->with('comments')->get();
        $res =  array();
        $res["fail"] = "0";
        $res["type"] = "add";
        $res["task"] = $task;
        return response($res, 200);
        
    }

    public function make_safe($variable) {
        $variable = strip_tags($variable);
        $variable = stripslashes($variable);
        $variable= trim($variable, "'");
          return $variable;
      }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
