<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $table = 'comments';
    public $timestamps = true;
    
    public function Tasks () {

        return $this->belongsTo('App\Task', 'task_id');
    }
}
