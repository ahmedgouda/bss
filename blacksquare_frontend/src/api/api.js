var baseUrl = 'http://localhost:8000'

const apiObject = {
  get_tasks: function (api) {
    return api.get(baseUrl + '/api/tasks')
  },
  add_task: function (api, $item) {
    return api.post(baseUrl + '/api/tasks', $item)
  },
  edit_task: function (api, $item) {
    return api.put(baseUrl + '/api/tasks/' + $item.id, $item)
  },
  delete_task: function (api, $id) {
    return api.delete(baseUrl + '/api/tasks/' + $id)
  },
  add_comment: function (api, $item) {
    return api.post(baseUrl + '/api/comments', $item)
  },
  resort_tasks: function (api, $items) {
    console.log($items)
    return api.post(baseUrl + '/api/sort_tasks', $items)
  }
}
export default apiObject
