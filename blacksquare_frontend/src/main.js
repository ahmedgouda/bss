// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueDraggable from 'vue-draggable'
import VueResource from 'vue-resource'
import VueProgressBar from 'vue-progressbar'
import VueLodash from 'vue-lodash'

const optionsLodash = { name: 'lodash' } // customize the way you want to call it
Vue.use(VueLodash, optionsLodash) // options is optional
const options = {
  color: '#007bff',
  failedColor: '#874b4b',
  thickness: '3px',
  location: 'top'
}

Vue.use(VueProgressBar, options)
Vue.use(VueResource)
Vue.use(VueDraggable)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
